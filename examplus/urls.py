from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.views.generic import RedirectView

from .routers import DefaultRouter
from courses.urls import router as course_router
from exams.urls import router as exam_router
from user_panel.urls import router as user_router
from user_panel.views.panel import index

router = DefaultRouter()
router.extend(exam_router)
router.extend(course_router)
router.extend(user_router)

urlpatterns = [
    url(r'^$', RedirectView.as_view(url='/exams/'), name='home'),
    url(r'^faq/$', index, name='faq'),
    url(r'^api/v1/', include(router.urls, namespace='api')),
    url(r'^admin/', admin.site.urls),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
    url(r'^exams/', include('exams.urls')),
    url(r'^courses/', include('courses.urls')),
    url(r'^user/', include('user_panel.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^grappelli/', include('grappelli.urls')),
]
