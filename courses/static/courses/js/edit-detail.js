$(document).ready(function() {
    var table = $('#students').DataTable({
        processing: true,
        serverSide: true,
        search: {
            search: $.urlParam('search')
        },
        ajax: {
            url: '/api/v1/students/',
            dataSrc: function(json) {
                json.data = json.results;
                json.recordsTotal = json.count;
                json.recordsFiltered = json.count;
                return json.data;
            },
            data: function(d) {
                x = {};
                x.course = course_id;
                x.limit = d.length;
                x.offset = d.start;
                x.ordering = (d.order[0].dir !== "asc" ? '-' : '') + d.columns[d.order[0].column].data;
                x.index = d.search.value;
                window.history.pushState('page2', 'Title', '?' + $.param({
                    'search': x.index
                }));
                return x;
            }
        },
        columns: [{
            data: "index",
            title: "Index"
        }, {
            data: "first_name",
            title: "Imię"
        }, {
            data: "last_name",
            title: "Nazwisko"
        }, {
            data: "is_added",
            title: "Dopisany",
            render: function(data, type, full, meta) {
                return data ? 'Tak': 'Nie';
            }
        }, {
            data: "is_blocked",
            title: "Zablokwoany",
            render: function(data, type, full, meta) {
                return data ? 'Tak': 'Nie';
            }
        }, {
            data: "id",
            render: function(data, type, full, meta) {
                return '<a class="signout_button" student_id="' + data + '" student_name="' + full.first_last_name + '">Edycja</a>';
            },
            title: "Edycja"
        }, ]
    });
    $('.popup_btn').popup({
        popup: $(this).find('.popup'),
        on: 'click',
        position: 'left center'
    });
    $('.update_param').click(function() {
        updateParam($(this));
    });
    $('.action > input').on('keyup', function (e) {
        if (e.keyCode == 13) {
            updateParam($(this));
        }
    });
    $('#students').on('click', '.signout_button', function(e) {
        $('#student_name').html($(this).attr('student_name'));
        $('#block_student').val($(this).attr('student_id'));
        $('#unblock_student').val($(this).attr('student_id'));
        $('#delete_student_modal').modal('show');
    });
    $('#block_student').click(function() {
        $.post(window.location.pathname, {
            key: $(this).attr('id'),
            data: $(this).val(),
            csrfmiddlewaretoken: csrftoken
        }, function(data) {
            table.draw();
        }, "json");
    });
    $('#unblock_student').click(function() {
        $.post(window.location.pathname, {
            key: $(this).attr('id'),
            data: $(this).val(),
            csrfmiddlewaretoken: csrftoken
        }, function(data) {
            table.draw();
        }, "json");
    });
    $('#add_student').click(function() {
        $.post(window.location.pathname, {
            csrfmiddlewaretoken: csrftoken,
            key: $(this).attr('id'),
            data: JSON.stringify($(this).closest('form').serializeObject())
        }, function(data) { table.draw(); }, "json");
    });
    $('#students_dropdown').dropdown({
        fields: {
            name: "first_last_name",
            value: "id"
        },
        apiSettings: {
            url: '/api/v1/students/?search={query}'
        },
        saveRemoteData: false
    });
    $('#id_course.dropdown').dropdown({
        fields: {
            name: "name",
            value: "id"
        },
        apiSettings: {
            url: '/api/v1/courses/?name={query}'
        },
        saveRemoteData: false
    });
    $('#delete_btn').click(function(){
        $('#delete_course_modal').modal('show');
    });
});
