$(document).ready(function() {
    var table = $('#courses').DataTable({
        processing: true,
        serverSide: true,
        search: {
            search: $.urlParam('search')
        },
        ajax: {
            url: '/api/v1/course_groups/',
            dataSrc: function(json) {
                json.data = json.results;
                json.recordsTotal = json.count;
                json.recordsFiltered = json.count;
                return json.data;
            },
            data: function(d) {
                x = {};
                x.limit = d.length;
                x.offset = d.start;
                x.ordering = (d.order[0].dir !== "asc" ? '-' : '') + d.columns[d.order[0].column].data;
                x.name = d.search.value;
                window.history.pushState('page2', 'Title', '?' + $.param({
                    'search': x.name
                }));
                return x;
            }
        },
        columns: [{
            data: "course_name",
            title: "Kurs"
        }, {
            data: "name",
            title: "Grupa"
        }, {
            data: "department",
            title: "Wydział"
        }, {
            data: "id",
            title: "Edycja",
            render: function(data, type, full, meta) {
                return '<a href="/courses/edit/' + data + '"> Edytuj </a>' ;
            }
        }, ]
    });
});
