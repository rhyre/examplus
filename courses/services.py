import csv
import hashlib
import re
from datetime import datetime

from .models import CourseGroup, Course, Department, Student


class StudentService(object):
    """docstring for StudentService"""

    @staticmethod
    def create_student(defaults, course_group, add_form=False):
        defaults['slug'] = hashlib.md5((defaults['index'] + str(datetime.now())).encode('utf-8')).hexdigest()
        defaults['course_group'] = course_group
        defaults['is_added'] = add_form
        Student(**defaults).save()

    @staticmethod
    def get_students_by_indexes(indexes):
        return Student.objects.filter(index__in=indexes)


class CourseService(object):
    """docstring for CourseService"""

    @staticmethod
    def add_group_from_csv(file, department_id, owner):
        def iso_8859_2_decoder(file):
            for line in file:
                yield line.decode('windows-1250')
        department = Department.objects.get(id=department_id)
        reader = csv.reader(iso_8859_2_decoder(file), delimiter=';')
        rows = [row for row in reader]
        params = {row[0]: row[1] for row in rows[2:10]}
        students = {
            re.search(r'(\d+)', row[1]).group(): {
                'last_name': row[2].strip(),
                'first_name': row[3].strip(),
                'index': re.search(r'(\d+)', row[1]).group(),
                'year': row[4].strip(),
                'semester': row[5].strip(),
                'course_of_working': row[6].strip(),
            } for row in rows[11:]}
        course, created = Course.objects.get_or_create(
            name=params['Kod kursu'],
            defaults={
                'name': params['Kod kursu'],
                'department': department,
            })
        course_group, created = CourseGroup.objects.get_or_create(
            course=course,
            defaults={
                'code': params['Kod grupy'],
                'course': course,
                'owner': owner,
                'semester': params['Semestr'],
                'year': params['Rok akademicki'],
                'calendar_type': params['Typ kalendarza'],
                'name': params['Nazwa kursu'],
                'date': params['Termin'],
                'teacher': params['Prowadzący'],
            })
        course_group.save()
        for index, student in students.items():
            StudentService.create_student(student, course_group)
        return course_group.id
