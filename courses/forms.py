from django import forms
from common.widgets import PopupText, PopupSelect
from .models import CourseGroup


class UploadFileForm(forms.Form):
    department = forms.IntegerField()
    file = forms.FileField()


class CourseGroupAddForm(forms.Form):
    course = forms.IntegerField()
    course_group_name = forms.CharField()


class CourseGroupEditForm(forms.ModelForm):
    course = forms.CharField(widget=PopupSelect(), max_length=200, label='Kurs')
    name = forms.CharField(widget=PopupText(attrs={'placeholder': 'Nazwa grupy...'}), max_length=200, label='Grupa')

    def __init__(self, *args, **kwargs):
        super(CourseGroupEditForm, self).__init__(*args, **kwargs)
        self.initial['course'] = self.instance.course.name

    class Meta:
        model = CourseGroup
        fields = ('name', 'course',)
