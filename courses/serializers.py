from rest_framework import serializers

from .models import Course, CourseGroup, Department, Student


class DepartmentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Department
        fields = ('id', 'name',)


class CourseAjaxSerializer(serializers.ModelSerializer):

    class Meta:
        model = Course
        fields = ('id', 'name',)


class CourseSerializer(serializers.ModelSerializer):
    department_name = serializers.CharField()

    class Meta:
        model = Course
        fields = ('id', 'name', 'department_name',)


class CourseGroupAjaxSerializer(serializers.ModelSerializer):
    class Meta:
        model = CourseGroup
        fields = ('id', 'name',)


class CourseGroupSerializer(serializers.ModelSerializer):
    course_name = serializers.CharField()
    department = serializers.CharField()

    class Meta:
        model = CourseGroup
        fields = ('id', 'name', 'course_name', 'department',)


class StudentSerializer(serializers.ModelSerializer):
    question_count = serializers.IntegerField()
    first_last_name = serializers.SerializerMethodField()

    class Meta:
        model = Student
        fields = ('id', 'first_name', 'last_name', 'index', 'question_count',
                  'first_last_name', 'slug', 'is_added', 'is_blocked',)

    def get_first_last_name(self, obj):
        return '%s %s' % (obj.first_name, obj.last_name)
