from django.contrib.auth import get_user
from django.db.models import F, Count, Q
from rest_framework import viewsets, filters
from rest_framework.decorators import list_route
from rest_framework.response import Response

from courses.models import Course, CourseGroup, Department, Student
from courses.serializers import CourseSerializer, CourseGroupSerializer, \
    DepartmentSerializer, CourseAjaxSerializer, CourseGroupAjaxSerializer, StudentSerializer


class CourseFilter(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        name = request.GET.get('name')
        if name:
            return queryset.filter(name__icontains=name)
        else:
            return queryset


class CourseGroupFilter(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        course = request.GET.get('name')
        if course:
            queryset = queryset.filter(course__name__icontains=course)
        return queryset


class DepartmentViewSet(viewsets.ModelViewSet):
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer
    filter_backends = (filters.OrderingFilter, CourseFilter)


class CourseViewSet(viewsets.ModelViewSet):
    queryset = Course.objects.annotate(
        department_name=F('department__name'))
    serializer_class = CourseSerializer
    filter_backends = (filters.OrderingFilter, CourseFilter)

    @list_route()
    def ajax(self, request):
        courses = Course.objects.all()
        if 'name' in request.GET:
            courses = courses.filter(name__icontains=request.GET['name'])
        serializer = CourseAjaxSerializer(courses, many=True)
        return Response(serializer.data)


class CourseGroupViewSet(viewsets.ModelViewSet):
    def get_queryset(self):
        return CourseGroup.objects.filter(owner=get_user(self.request)).annotate(
            course_name=F('course__name'),
            department=F('course__department__name'))
    serializer_class = CourseGroupSerializer
    filter_backends = (filters.OrderingFilter, CourseGroupFilter)

    @list_route()
    def ajax(self, request):
        course_gropus = CourseGroup.objects.all()
        if 'name' in request.GET:
            course_gropus = course_gropus.filter(name__icontains=request.GET['name'])
        serializer = CourseGroupAjaxSerializer(course_gropus, many=True)
        return Response({'results': serializer.data})


class StudentFilter(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        course_group_id = request.GET.get('course')
        exam_id = request.GET.get('exam')
        index = request.GET.get('index')
        search = request.GET.get('search')
        if search:
            queryset = queryset.filter(
                Q(first_name__icontains=search) |
                Q(last_name__icontains=search) |
                Q(index__icontains=search))
        if index:
            queryset = queryset.filter(index__icontains=index)
        if course_group_id:
            queryset = queryset.filter(course_group__id=course_group_id)
        if exam_id:
            queryset = queryset.filter(exams__id=exam_id)
        return queryset


class StudentViewSet(viewsets.ModelViewSet):
    queryset = Student.objects.annotate(
        question_count=Count('answers'))

    serializer_class = StudentSerializer
    filter_backends = (filters.OrderingFilter, StudentFilter)
