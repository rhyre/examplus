import json

from django.contrib.auth import get_user
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404

from courses.forms import UploadFileForm, CourseGroupEditForm, CourseGroupAddForm
from courses.models import CourseGroup, Course, Student
from courses.services import StudentService, CourseService


@login_required
def search(request):
    if request.method == 'POST':
        user = get_user(request)
        if 'data' in request.POST and 'sign' in request.POST:
            sign = request.POST['sign']
            course = CourseGroup.objects.get(id=request.POST['data'])
            if int(sign):
                course.owner = user
            else:
                course.owner = None
        return JsonResponse({"success": "1", })
    return render(request, 'courses/search/index.html')


@login_required
def add(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            course_id = CourseService.add_group_from_csv(
                request.FILES['file'],
                form.cleaned_data['department'],
                get_user(request))
            return redirect('courses:edit_detail', course_id=course_id)
        form = CourseGroupAddForm(request.POST)
        if form.is_valid():
            course = Course.objects.get(pk=form.cleaned_data['course'])
            cg = CourseGroup(name=form.cleaned_data['course_group_name'], course=course, owner=get_user(request))
            cg.save()
            return redirect('courses:edit_detail', course_id=cg.id)
    return render(request, 'courses/add/index.html')


@login_required
def edit_detail(request, course_id):
    course_group = get_object_or_404(CourseGroup, pk=course_id)
    if request.method == 'POST':
        if 'key' in request.POST and 'data' in request.POST:
            key = request.POST['key']
            changed = ''
            if key == 'add_student':
                student = json.loads(request.POST['data'])
                StudentService.create_student(student, course_group, add_form=True)
            elif key == 'block_student':
                Student.objects.filter(id=int(request.POST['data'])).update(is_blocked=True)
            elif key == 'unblock_student':
                Student.objects.filter(id=int(request.POST['data'])).update(is_blocked=False)
            elif key == 'course':
                course = Course.objects.get(id=request.POST['data'])
                course_group.course = course
                course_group.save()
                changed = course.name
            elif key == 'name':
                course_group.name = request.POST['data']
                course_group.save()
                changed = course_group.name
        return JsonResponse({"success": "1", "changed": changed})
    form = CourseGroupEditForm(instance=course_group)
    return render(request, 'courses/edit_detail/index.html', {'course_group': course_group, 'form': form})


@login_required
def delete_course_group(request, course_group_id):
    cg = get_object_or_404(CourseGroup, pk=course_group_id)
    cg.delete()
    return redirect('courses:search')
