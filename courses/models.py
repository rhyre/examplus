from django.contrib.auth.models import User
from django.db import models


class Department(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        verbose_name = "Department"
        verbose_name_plural = "Departments"

    def __str__(self):
        return self.name


class Course(models.Model):
    name = models.CharField(max_length=200)
    department = models.ForeignKey(Department, related_name='courses', on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Course"
        verbose_name_plural = "Courses"

    def __str__(self):
        return self.name


class CourseGroup(models.Model):
    name = models.CharField(max_length=200)
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='course_groups')
    owner = models.ForeignKey(User, related_name='course_groups', on_delete=models.CASCADE)
    semester = models.CharField(max_length=200, null=True)
    calendar_type = models.CharField(max_length=200, null=True)
    date = models.CharField(max_length=200, null=True)
    year = models.CharField(max_length=200, null=True)
    code = models.CharField(max_length=200, null=True)
    teacher = models.CharField(max_length=200, null=True)

    class Meta:
        verbose_name = "CourseGroup"
        verbose_name_plural = "CourseGroups"

    def __str__(self):
        return self.name


class Student(models.Model):
    index = models.CharField(max_length=200)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    course_group = models.ForeignKey(CourseGroup, on_delete=models.CASCADE, related_name='studnets')
    slug = models.SlugField(unique=True)
    is_added = models.BooleanField(default=False)
    is_blocked = models.BooleanField(default=False)
    year = models.CharField(max_length=200, null=True)
    semester = models.CharField(max_length=200, null=True)
    course_of_working = models.CharField(max_length=200, null=True)

    class Meta:
        verbose_name = "Student"
        verbose_name_plural = "Students"

    def __str__(self):
        return self.index
