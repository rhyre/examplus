from django.conf.urls import url
from rest_framework import routers

from .views import courses_views, api


app_name = 'courses'
router = routers.DefaultRouter()
router.register(r'courses', api.CourseViewSet)
router.register(r'departments', api.DepartmentViewSet)
router.register(r'course_groups', api.CourseGroupViewSet, base_name='course_group')
router.register(r'students', api.StudentViewSet)

urlpatterns = [
    url(r'^$', courses_views.search, name='search'),
    url(r'^delete/(?P<course_group_id>[\d]+)/$', courses_views.delete_course_group, name='delete'),
    url(r'^add/$', courses_views.add, name='add'),
    url(r'^edit/(?P<course_id>[0-9]+)/$', courses_views.edit_detail, name='edit_detail'),
]
