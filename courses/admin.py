from django.contrib import admin

from .models import Course, Department, CourseGroup, Student


admin.site.register(Student)
admin.site.register(Course)
admin.site.register(Department)
admin.site.register(CourseGroup)
