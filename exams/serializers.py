from rest_framework import serializers

from .models import Exam, QuestionSet, Question, ExamSlug


class ExamSerializer(serializers.ModelSerializer):
    course_name = serializers.CharField()
    department = serializers.CharField()
    start_date = serializers.DateTimeField(format="%Y-%m-%d %H:%M")

    class Meta:
        model = Exam
        fields = ('id', 'name', 'course_name', 'department', 'start_date', 'class_room', 'can_start')


class ExamSlugSerializer(serializers.ModelSerializer):
    student_first_name = serializers.CharField()
    student_index = serializers.CharField()
    student_last_name = serializers.CharField()
    answers_sum = serializers.IntegerField()

    class Meta:
        model = ExamSlug
        fields = ('id', 'student_first_name', 'student_index', 'student_last_name', 'status', 'answers_sum')


class QuestionSetSerializer(serializers.ModelSerializer):

    class Meta:
        model = QuestionSet
        fields = ('id', 'name',)


class QuestionSetAjaxSerializer(serializers.ModelSerializer):

    class Meta:
        model = QuestionSet
        fields = ('id', 'name',)


class QuestionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Question
        fields = ('id', 'description', 'typ', 'group', 'nr', 'choices')
