from django.conf.urls import url
from rest_framework import routers

from .views import exams_views, api, questions_views


app_name = 'exams'
router = routers.DefaultRouter()
router.register(r'exams', api.ExamsViewSet, base_name='exams')
router.register(r'question_set', api.QuestionSetViewSet)
router.register(r'question', api.QuestionViewSet)
router.register(r'exam_slugs', api.ExamsSlugViewSet)

urlpatterns = [
    url(r'^$', exams_views.search, name='search'),
    url(r'^edit/(?P<exam_id>[0-9]+)/$', exams_views.edit_detail, name='edit_detail'),
    url(r'^get/csv/(?P<exam>[0-9]+)/$', exams_views.download_exam_csv, name='download'),
    url(r'^start/(?P<exam_id>[0-9]+)/$', exams_views.start_exam, name='start_exam'),
    url(r'^panel/(?P<exam_id>[0-9]+)/$', exams_views.start_exam_panel, name='start_exam_panel'),
    url(r'^execution/(?P<exam_id>[0-9]+)/$', exams_views.execution, name='execution'),
    url(r'^add/$', exams_views.add, name='add'),
    url(r'^student/(?P<slug>[\w]+)/$', exams_views.student_panel, name='student_panel'),
    url(r'^student/(?P<slug_student>[\w]+)/exam/(?P<slug_exam>[\w]+)/$', exams_views.exam_panel, name='student_exam_panel'),
    url(r'^student/(?P<slug_student>[\w]+)/exam/(?P<slug_exam>[\w]+)/start/$', exams_views.start_exam_student, name='student_exam_start'),
    url(r'^student/(?P<slug_student>[\w]+)/exam/(?P<slug_exam>[\w]+)/end/$', exams_views.end_exam, name='student_exam_end'),
    url(r'^student/(?P<slug_student>[\w]+)/exam/(?P<slug_exam>[\w]+)/(?P<nr>[\d]+)/$', exams_views.exam_question, name='exam_question'),
    url(r'^questions/add/$', questions_views.add, name='question_add'),
    url(r'^questions/search/$', questions_views.search, name='question_search'),
    url(r'^questions/edit/(?P<question_set>[0-9]+)/$', questions_views.edit, name='question_edit'),
    url(r'^delete/(?P<exam_id>[\d]+)/$', exams_views.delete_exam, name='delete'),
]
