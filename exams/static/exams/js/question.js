$(document).ready(function() {

  $('.math-render').each(function(index){
      $(this).html($(this).html().replace(/\$(.*?)\$/g, function (outer, inner) {return katex.renderToString(inner);}));
  });
  $('.checkbox').checkbox({
    onChecked: function() {
      name = $(this).closest(".ui.checkbox").find('input').attr('hash');
      $.post(window.location.pathname, {
            key: 'check',
            data: name,
            csrfmiddlewaretoken: csrftoken
        }, function(data) {
           console.log('selected check', name);
        }, "json");
    },
    onUnchecked: function() {
      // name = $(this).closest(".ui.checkbox").find('input').attr('name');
      // $.post(window.location.pathname, {
      //       key: 'uncheck',
      //       data: name,
      //       csrfmiddlewaretoken: csrftoken
      //   }, function(data) {
      //      console.log('selected uncheck', name);
      //   }, "json");
    }});
});
