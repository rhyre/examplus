$(document).ready(function() {
    var table = $('#students').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '/api/v1/exam_slugs/',
            dataSrc: function(json) {
                json.data = json.results;
                json.recordsTotal = json.count;
                json.recordsFiltered = json.count;
                return json.data;
            },
            data: function(d) {
                x = {};
                x.exam = exam_id;
                x.limit = d.length;
                x.offset = d.start;
                x.ordering = (d.order[0].dir !== "asc" ? '-' : '') + d.columns[d.order[0].column].data;
                x.name = d.search.value;
                return x;
            }
        },
        columns: [{
            data: "student_first_name",
            title: "Imię"
        }, {
            data: "student_last_name",
            title: "Nazwisko"
        }, {
            data: "student_index",
            title: "Index",
        }, {
            data: "id",
            title: "Obecny",
            render: function(data, type, full, meta) {
                if (full.status === 0){
                    return '<a class="edit_button" value="' + data + '"">Zatwierdź</a>';
                } else {
                    return 'Obecny - <a class="edit_button" value="' + data + '"">Usuń</a>';
                }
            }
        }, ]
    });
    $('#students').on('click', '.edit_button', function(e) {
        exam_slug_id = $(this).attr('value');
        $.post(window.location.pathname, {
            exam_slug_id: exam_slug_id,
            csrfmiddlewaretoken: csrftoken
        }, function(data) {
            table.draw();
        }, "json");
    });
    $('#add_all').click(function() {
        $.post(window.location.pathname, {
            add_all: 1,
            csrfmiddlewaretoken: csrftoken
        }, function(data) {
            table.draw();
        }, "json");
    });
});
