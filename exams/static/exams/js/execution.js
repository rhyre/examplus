$(document).ready(function() {
    var table = $('#students').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '/api/v1/exam_slugs/',
            dataSrc: function(json) {
                json.data = json.results;
                json.recordsTotal = json.count;
                json.recordsFiltered = json.count;
                return json.data;
            },
            data: function(d) {
                x = {};
                x.exam = exam_id;
                x.limit = d.length;
                x.offset = d.start;
                x.ordering = (d.order[0].dir !== "asc" ? '-' : '') + d.columns[d.order[0].column].data;
                x.name = d.search.value;
                x.in_progress = 1;
                return x;
            }
        },
        columns: [{
            data: "student_first_name",
            title: "Imię"
        }, {
            data: "student_last_name",
            title: "Nazwisko"
        }, {
            data: "student_index",
            title: "Index",
        }, {
            data: "status",
            title: "Obecny",
            render: function(data, type, full, meta) {
                if (data === 1){
                    return '<p>W trakcie</p>';
                } else  if (data === 2) {
                    return '<p>Ukończony</p>';
                }
                return '-';
            }
        },{
            data: "answers_sum",
            title: "Ocena",
            render: function(data, type, full, meta) {
                percentage = data*100/max_points;
                switch (true) {
                    case percentage >= exam_grades['3.0'] && percentage < exam_grades['3.5']:
                        grade = '3.0';
                        break;
                    case percentage >= exam_grades['3.5'] && percentage < exam_grades['4.0']:
                        grade = '3.5';
                        break;
                    case percentage >= exam_grades['4.0'] && percentage < exam_grades['4.5']:
                        grade = '4.0';
                        break;
                    case percentage >= exam_grades['4.5'] && percentage < exam_grades['5.0']:
                        grade = '4.5';
                        break;
                    case percentage >= exam_grades['5.0'] && percentage < exam_grades['5.5']:
                        grade = '5.0';
                        break;
                    case percentage >= exam_grades['5.5']:
                        grade = '5.5';
                        break;
                  default:
                     grade = '2.0';
                }
                return grade;
            }
        }, ]
    });
});
