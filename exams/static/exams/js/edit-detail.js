$(document).ready(function() {
    var table = $('#students').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '/api/v1/students/',
            dataSrc: function(json) {
                json.data = json.results;
                json.recordsTotal = json.count;
                json.recordsFiltered = json.count;
                return json.data;
            },
            data: function(d) {
                x = {};
                x.exam = exam_id;
                x.limit = d.length;
                x.offset = d.start;
                x.ordering = (d.order[0].dir !== "asc" ? '-' : '') + d.columns[d.order[0].column].data;
                x.index = d.search.value;
                return x;
            }
        },
        columns: [{
            data: "index",
            title: "Index",
            render: function(data, type, full, meta) {
                return '<a href="/exams/student/' + full.slug + '"/>' + data + '</a>';
            }
        }, {
            data: "first_name",
            title: "Imię"
        }, {
            data: "last_name",
            title: "Nazwisko"
        }, {
            data: "question_count",
            title: "Egazmin",
            render: function(data, type, full, meta) {
                return data ? 'Wygenerowany' : 'Nie wygenerowany';
            }
        }, {
            data: "id",
            title: "Wypisz",
            render: function(data, type, full, meta) {
                return '<a class="signout_button" student_id="' + data + '" student_name="' + full.first_last_name + '">Wypisz</a>';
            }
        }, ]
    });
    $('#students').on('click', '.signout_button', function(e) {
        $('#student_name').html($(this).attr('student_name'));
        $('#remove_student').attr('data', $(this).attr('student_id'));
        $('#delete_student_modal').modal('show');
    });
    $('#add_student_single').click(function() {
        $.post(window.location.pathname, {
            key: $(this).attr('id'),
            data: $('#students_dropdown>input').val(),
            csrfmiddlewaretoken: csrftoken
        }, function(data) {
            table.draw();
        }, "json");
    });
    $('#delete_exam').click(function() {
        $('#delete_exam_modal').modal('show');
    });
    $('.popup_btn').popup({
        popup: $(this).find('.popup'),
        on: 'click',
        position: 'left center'
    });
    $('.update_btn').click(function() {
        key = $(this).attr('id');
        data = $(this).attr('data') | 0;
        $.post(window.location.pathname, {
            key: key,
            data: data,
            csrfmiddlewaretoken: csrftoken
        }, function(data) {
            table.draw();
        }, "json");
    });
    $('.update_param').click(function() {
        updateParam($(this), 'update_');
    });
    $('.action > input').on('keyup', function (e) {
        if (e.keyCode == 13) {
            updateParam($(this), 'update_');
        }
    });
    $('#students_dropdown').dropdown({
        fields: {
            name: "first_last_name",
            value: "id"
        },
        apiSettings: {
            url: '/api/v1/students/?search={query}'
        },
        saveRemoteData: false
    });
    $('#id_course_group').dropdown({
        fields: {
            name: "name",
            value: "id"
        },
        apiSettings: {
            url: '/api/v1/course_groups/ajax/?name={query}'
        },
        saveRemoteData: false
    });
    $('.ui.calendar').calendar({
        ampm: false,
    });
    $('#send_links').click(function() {
        $(this).attr("disabled", 1);
        key = $(this).attr('id');
        data = $(this).attr('data') | 0;
        $.post(window.location.pathname, {
            key: key,
            data: data,
            csrfmiddlewaretoken: csrftoken
        }, function(data) {
            alert('Wysłano linki');
        }, "json");
    });
});
