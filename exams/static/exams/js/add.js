$(document).ready(function() {
    $('#course').dropdown({
        fields: {
            name: "name",
            value: "id"
        },
        apiSettings: {
            url: '/api/v1/course_groups/ajax/?name={query}'
        },
        saveRemoteData: false
    });
    $('#department').dropdown({
        fields: {
            name: "name",
            value: "id"
        },
        apiSettings: {
            url: '/api/v1/departments/?name={query}'
        },
        saveRemoteData: false
    });
    $('#question_set').dropdown({
        fields: {
            name: "name",
            value: "id"
        },
        apiSettings: {
            url: '/api/v1/question_set/ajax/?name={query}'
        },
        saveRemoteData: false
    });
    $('#date_calendar').calendar({
        ampm: false,
    });
});
