$(document).ready(function() {
    var table = $('#exams').DataTable({
        processing: true,
        serverSide: true,
        search: {
            search: $.urlParam('search')
        },
        ajax: {
            url: '/api/v1/exams/',
            dataSrc: function(json) {
                json.data = json.results;
                json.recordsTotal = json.count;
                json.recordsFiltered = json.count;
                return json.data;
            },
            data: function(d) {
                x = {};
                x.limit = d.length;
                x.offset = d.start;
                x.ordering = (d.order[0].dir !== "asc" ? '-' : '') + d.columns[d.order[0].column].data;
                x.name = d.search.value;
                window.history.pushState('page2', 'Title', '?' + $.param({
                    'search': x.name
                }));
                return x;
            }
        },
        columns: [{
            data: "name",
            title: "Egzamin"
        }, {
            data: "department",
            title: "Wydział"
        }, {
            data: "course_name",
            title: "Kurs"
        }, {
            data: "start_date",
            title: "Data"
        }, {
            data: "class_room",
            title: "Sala"
        }, {
            data: "id",
            render: function(data, type, full, meta) {
                if (full.can_start){
                    return '<a href="/exams/execution/' + data + '"> Raport </a>';
                } else {
                    return '<a href="/exams/panel/' + data + '"> Start </a>';
                }
            },
            width: "10ch",
            title: "Panel"
        },{
            data: "id",
            render: function(data, type, full, meta) {
                return '<a href="/exams/edit/' + data + '"> Edytuj </a>';
            },
            width: "10ch",
            title: "Edycja"
        }, ]
    });
});
