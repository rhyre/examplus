$(document).ready(function() {
    $('#filename')
        .on('click', function(e) {
            $('#fileup').click();
        });
    $('#fileup')
        .on('change', function(e) {
            var name = e.target.files[0].name;
            $('#filename').val(name);
        });
    $('#course').dropdown({
        fields: {
            name: "name",
            value: "id"
        },
        apiSettings: {
            url: '/api/v1/courses/?name={query}'
        },
        saveRemoteData: false
    });
    $('#department').dropdown({
        fields: {
            name: "name",
            value: "id"
        },
        apiSettings: {
            url: '/api/v1/departments/?name={query}'
        },
        saveRemoteData: false
    });
});
