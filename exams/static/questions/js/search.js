$(document).ready(function() {
    $('#questions').dataTable({
        processing: true,
        serverSide: true,
        search: {
            search: $.urlParam('search')
        },
        ajax: {
            url: '/api/v1/question_set/',
            dataSrc: function(json) {
                json.data = json.results;
                json.recordsTotal = json.count;
                json.recordsFiltered = json.count;
                return json.data;
            },
            data: function(d) {
                x = {};
                x.limit = d.length;
                x.offset = d.start;
                x.ordering = (d.order[0].dir !== "asc" ? '-' : '') + d.columns[d.order[0].column].data;
                x.name = d.search.value;
                window.history.pushState('page2', 'Title', '?' + $.param({
                    'search': x.name
                }));
                return x;
            }
        },
        columns: [{
            data: "name",
            title: "Nazwa"
        }, {
            data: "name",
            title: "Kursy"
        }, {
            data: "name",
            title: "Wydziały"
        }, {
            data: "name",
            title: "Data"
        }, {
            data: "id",
            render: function(data, type, full, meta) {
                return '<a href="/exams/questions/edit/' + data + '/">Edytuj</a>';
            },
            width: "10ch",
            title: "Edycja"
        }, ]
    });
});
