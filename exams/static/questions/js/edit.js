$(document).ready(function() {
    var table = $('#questions').DataTable({
        processing: true,
        serverSide: true,
        search: {
            search: $.urlParam('search')
        },
        ajax: {
            url: '/api/v1/question/',
            dataSrc: function(json) {
                json.data = json.results;
                json.recordsTotal = json.count;
                json.recordsFiltered = json.count;
                return json.data;
            },
            data: function(d) {
                x = {};
                x.question_set = question_set;
                x.limit = d.length;
                x.offset = d.start;
                x.ordering = (d.order[0].dir !== "asc" ? '-' : '') + d.columns[d.order[0].column].data;
                x.description = d.search.value;
                window.history.pushState('page2', 'Title', '?' + $.param({
                    'search': x.description
                }));
                return x;
            }
        },
        columns: [{
            data: "nr",
            title: "Lp."
        }, {
            data: "description",
            title: "Pytanie",
            render: function(data, type, full, meta) {
                return data.replace(/\$(.*?)\$/g, function (outer, inner) {
                  return katex.renderToString(inner);
                });
            },
        }, {
            data: "id",
            render: function(data, type, full, meta) {
                return '<a class="edit_button" value="' + data + '">Edytuj</a>';
            },
            width: "10ch",
            title: "Edytuj"
        }, ],
    });
    $('#questions').on('click', '.edit_button', function(e) {
        qid = $(this).attr('value');
        $.get('/api/v1/question/' + qid, {
            question_set: question_set,
        }, function(data) {
            $('#answers .field').remove();
            $('#question_id').val(qid);
            $('#description').val(data.description);
            $('#group').val(data.group);
            $('#typ').val(data.typ);
            if(data.choices){
                choices = JSON.parse(data.choices.replace(/'/g, '"'));
                for (var key in choices) {
                  if (choices.hasOwnProperty(key)) {
                    add_choice(choices[key].answer, choices[key].points);
                  }
                }
            }
            $('.modal').modal('show');
        }, "json");
    });
    $('#add_new').click(function() {
        $('#question_id').val('');
        $('#description').val('');
        $('#group').val('');
        $('#typ').val('');
        $('#choices').val('');
        $('.modal').modal('show');
    });
    $('#save_btn').click(function() {
        var choices = {};
        $(".answers").each(function( index ) {
            choices[index + 1] = {
                answer: $(this).find('.answer_field').val(),
                points: $(this).find('.dropdown.button').dropdown('get value')
            };
        });
        $.post(window.location.pathname, {
            description: $('#description').val(),
            group: $('#group').val(),
            question_id: $('#question_id').val(),
            choices: JSON.stringify(choices),
            typ: $('#typ').val(),
            csrfmiddlewaretoken: csrftoken
        }, function(data) {
            table.draw();
        }, "json");
    });
    $('.ui.dropdown').dropdown({});
    function add_choice(answer, points) {
        count = ($('#answers > .field').length + 1);
        field = `
          <div class="field">
            <label>Odpowiedź ` + count + `</label>
            <div class="ui input">
              <div class="ui right action input answers">
                <input class="answer_field" id="answer` + count + `" type="text" placeholder="Odpowiedź.. " value="` + answer + `">
                <div id="score` + count + `" class="ui selection dropdown button">
                  <input type="hidden" name="score">
                  <i class="dropdown icon"></i>
                  <div class="default text">Liczba pkt</div>
                  <div class="menu">
                    <div class="item" data-value="1">1 pkt</div>
                    <div class="item" data-value="0">0 pkt</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
            `;
        $('#answers').append(field);
        $('#score' + count).dropdown('set selected', '' + points);
    }
    $('#add_choice').click(function(){
        add_choice('', undefined);
        return false;
    });
    $('#del_choice').click(function(){
        $('#answers .field:last-child').remove();
        return false;
    });
});
