from django.contrib.auth.models import User
from django.db import models
from jsonfield import JSONField

from courses.models import CourseGroup, Course, Student


class QuestionSet(models.Model):
    name = models.CharField(max_length=200)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='question_sets')
    course = models.ManyToManyField(Course, related_name='question_sets')

    class Meta:
        verbose_name = "QuestionSet"
        verbose_name_plural = "QuestionSets"

    def __str__(self):
        return self.name


class Question(models.Model):
    description = models.TextField(max_length=4000)
    choices = JSONField()
    question_set = models.ForeignKey(QuestionSet, on_delete=models.CASCADE, related_name='questions')
    group = models.IntegerField()
    picture = models.ImageField(null=True, blank=True)
    typ = models.IntegerField()
    nr = models.IntegerField()

    class Meta:
        verbose_name = "Question"
        verbose_name_plural = "Questions"

    def __str__(self):
        return self.description


class Exam(models.Model):
    name = models.CharField(max_length=200)
    course_group = models.ForeignKey(CourseGroup, related_name='exams', on_delete=models.CASCADE)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField(null=True, blank=True)
    question_set = models.ForeignKey(QuestionSet, related_name='exams', on_delete=models.CASCADE)
    duration = models.IntegerField(default=30)
    question_count = models.IntegerField(default=20)
    students = models.ManyToManyField(Student, related_name='exams')
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='exams')
    grade = JSONField(default={'3.0': 50, '3.5': 60, '4.0': 70, '4.5': 80, '5.0': 90, '5.5': 95})
    class_room = models.CharField(max_length=200, blank=True, null=True)
    can_start = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)
    exam_start_date = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = "Exam"
        verbose_name_plural = "Exams"

    def __str__(self):
        return self.name


class ExamSlug(models.Model):
    NEW = 0
    IN_PROGRESS = 1
    FINISHED = 2
    NOT_PRESENT = 3
    STATUSES = (NEW, 'Nowy'), (IN_PROGRESS, 'W Trakcie'), (FINISHED, 'Ukończony'), (NOT_PRESENT, 'Nieobecny')

    slug = models.SlugField()
    exam = models.ForeignKey(Exam, related_name='slugs', on_delete=models.CASCADE)
    student = models.ForeignKey(Student, related_name='exam_slugs', on_delete=models.CASCADE)
    status = models.IntegerField(choices=STATUSES, default=NEW)
    email_sent = models.BooleanField(default=False)
    end_date = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = "ExamSlug"
        verbose_name_plural = "ExamSlugs"

    def __str__(self):
        return self.slug


class Answer(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name='answers')
    exam = models.ForeignKey(Exam, on_delete=models.CASCADE, related_name='answers')
    exam_slug = models.ForeignKey(ExamSlug, on_delete=models.CASCADE, related_name='answers')
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='answers')
    answer = models.CharField(max_length=255, default='')
    result = models.IntegerField(default=0)

    class Meta:
        verbose_name = "Answer"
        verbose_name_plural = "Answers"

    def __str__(self):
        return str(self.result)
