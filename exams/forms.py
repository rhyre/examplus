from django import forms

from .models import Exam
from common.widgets import PopupText, PopupDateTime, PopupSelect


class UploadFileForm(forms.Form):
    question_set = forms.CharField()
    course = forms.IntegerField()
    department = forms.IntegerField()
    file = forms.FileField()


class AddExamForm(forms.ModelForm):

    start_date = forms.DateTimeField(input_formats=['%B %d, %Y %H:%M'])

    class Meta:
        model = Exam
        fields = ('name', 'course_group', 'question_set', 'start_date', 'class_room')


class ExamEditForm(forms.ModelForm):
    name = forms.CharField(widget=PopupText(attrs={'placeholder': 'Nazwa egzaminu...'}), max_length=200, label='Nazwa')
    class_room = forms.CharField(widget=PopupText(attrs={'placeholder': 'Nazwa sali...'}), max_length=200, label='Sala')
    course_group = forms.CharField(widget=PopupSelect(), max_length=200, label='Grupa')
    duration = forms.CharField(widget=PopupText(attrs={'placeholder': 'Czas egzaminu...'}), max_length=200, label='Czas trwania')
    question_count = forms.CharField(widget=PopupText(attrs={'placeholder': 'Liczba pytań...'}), max_length=200, label='Liczba pytań')
    start_date = forms.CharField(widget=PopupDateTime(attrs={'placeholder': 'Data egzaminu...'}), max_length=200, label='Termin')

    def __init__(self, *args, **kwargs):
        super(ExamEditForm, self).__init__(*args, **kwargs)
        self.initial['course_group'] = self.instance.course_group.name
        self.initial['start_date'] = self.instance.start_date.strftime('%B %-d, %Y %H:%M')

    class Meta:
        model = Exam
        fields = ('name', 'class_room', 'duration', 'course_group', 'question_count', 'start_date')
