import hashlib
import os
from datetime import datetime
from dbfread import DBF
from .models import QuestionSet, Question, Answer, ExamSlug, Exam
from django.http import HttpResponse
from django.template import loader
from django.http import Http404


class ExamService(object):
    """docstring for ExamService"""

    @staticmethod
    def add_exam_from_dbf(teacher, file, department_id, course, question_set_name):
        path = '/tmp/' + hashlib.md5(str(datetime.now()).encode('utf-8')).hexdigest()
        with open(path, 'wb+') as destination:
            for chunk in file.chunks():
                destination.write(chunk)
        question_set = QuestionSet(name=question_set_name, owner=teacher)
        question_set.save()
        question_set.course.add(course)
        table = DBF(path, encoding='Windows-1250')
        for rec in table:
            q = Question(
                description=rec['PYTANIE'],
                question_set=question_set,
                group=rec['NR_GRUPY'],
                typ=rec['TYP_PYT'],
                nr=rec['NR_PYT'])
            answers = ['A', 'B', 'C', 'D', 'E', 'F']
            choices = {}
            for i, a in enumerate(answers):
                if rec['ODP_%s' % a]:
                    key = hashlib.md5(str(datetime.now()).encode('utf-8') + str(a).encode('utf-8')).hexdigest()
                    choices[key[:8]] = {'id': i, 'points': rec['PUNKTY_%s' % a], 'answer': rec['ODP_%s' % a]}
            q.choices = choices
            q.save()
        os.remove(path)
        return question_set

    @staticmethod
    def _generate(student, exam, question_list):
        import random
        count = exam.question_count
        slug = hashlib.md5((str(datetime.now()) + student.index).encode('utf-8')).hexdigest()
        exam_slug = ExamSlug(slug=slug, exam=exam, student=student)
        exam_slug.save()
        questions = random.sample(question_list, count)
        for question in questions:
            Answer(student=student, exam=exam, question=question, exam_slug=exam_slug).save()

    @staticmethod
    def generate_exams(exam):
        Answer.objects.filter(exam=exam).delete()
        students = exam.students
        question_list = set(Question.objects.filter(question_set=exam.question_set))
        for student in students.all():
            ExamService._generate(student, exam, question_list)

    @staticmethod
    def get_exam_csv(exam):
        exam = Exam.objects.get(id=exam)
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="raport.csv"'
        csv_data = (
            ('',),
            ('Politechnika Wrocławska',),
            ('Rok akademicki', exam.course_group.year),
            ('Typ kalendarza', exam.course_group.calendar_type),
            ('Semestr', exam.course_group.semester),
            ('Kod grupy', exam.course_group.code),
            ('Kod kursu', exam.course_group.course.name),
            ('Nazwa kursu', exam.course_group.name),
            ('Termin', exam.course_group.date,),
            ('Prowadzący', exam.course_group.teacher,),
            ('Lp.', 'Nr albumu', 'Nazwisko', 'Imiona', 'Rok', 'Semestr',
                'Przedmiot kształcenia', 'Ocena (np. 3.0)', 'Data (RRRR-MM-DD)', 'Komentarz',),
        )

        students_list = exam.students.all()
        max_points = exam.question_count
        for student in students_list:
            percent_score = (Answer.objects.filter(result__gt=0, exam=exam, student=student).count() / max_points) * 100
            try:
                items = sorted({k: v for k, v in exam.grade.items() if v <= percent_score})
            except AttributeError:
                raise Http404("Grade error")
            student.grade = '2.0' if not items else items[-1]
        students = ((s.index, s.last_name, s.first_name, s.year, s.semester, s.course_of_working, s.grade, '', '',) for s in students_list)

        t = loader.get_template('exams/csv/index.html')
        c = {
            'data': csv_data,
            'students': students,
        }
        response.write(t.render(c))
        return response
