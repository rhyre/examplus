import logging
from datetime import datetime

from django.conf import settings
from django.contrib.auth import get_user
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.db.models import F, Case, Sum, When, IntegerField
from django.http import JsonResponse, Http404
from django.shortcuts import render, redirect, get_object_or_404, reverse

from courses.models import CourseGroup, Student
from exams.forms import AddExamForm, ExamEditForm
from exams.models import Exam, ExamSlug, Answer
from exams.services import ExamService


logger = logging.getLogger(__name__)


@login_required
def search(request):
    return render(request, 'exams/search/index.html')


@login_required
def add(request):
    if request.method == 'POST':
        form = AddExamForm(request.POST)
        if form.is_valid():
            new_exam = form.save(commit=False)
            new_exam.owner = get_user(request)
            new_exam.save()
            return redirect('exams:edit_detail', new_exam.id)
    return render(request, 'exams/add/index.html')


@login_required
def edit_detail(request, exam_id):
    exam = get_object_or_404(Exam, pk=exam_id)
    if request.method == 'POST':
        if 'key' in request.POST and 'data' in request.POST:
            key = request.POST['key']
            data = request.POST['data']
            changed = ''
            if key.startswith('update'):
                if '_grade' in key:
                    exam.grade[key[13:]] = int(data)
                    exam.save()
                    changed = ''
                    grades = sorted(exam.grade.items())
                    last = grades[-1]
                    for grade, score in sorted(exam.grade.items()):
                        changed += grade + ': ' + str(score) + '% '
                        if grade != last[0]:
                            changed += ' | '
                    return JsonResponse({"success": "1", "changed": changed})
                if '_date' in key:
                    data = datetime.strptime(data, '%B %d, %Y %H:%M')
                    changed = data.strftime('%B %-d, %Y %H:%M')
                elif 'course_group' in key:
                    data = get_object_or_404(CourseGroup, pk=data)
                    changed = data.name
                setattr(exam, key[7:], data)
                exam.save()
                changed = getattr(exam, key[7:]) if not changed else changed
            elif key == 'add_student_single':
                exam.students.add(int(data))
            elif key == 'remove_student':
                exam.students.remove(int(data))
                ExamSlug.objects.filter(exam=exam, student__id=data).delete()
                Answer.objects.filter(exam=exam, student__id=data).delete()
            elif key == 'generate':
                ExamService.generate_exams(exam)
            elif key == 'send_links':
                slugs = exam.slugs.filter(student__in=exam.students.all(), email_sent=False)
                for slug in slugs:
                    link = reverse('exams:student_exam_panel', args=[slug.student.slug, slug.slug])
                    recip = slug.student.index[-6:] + settings.PWR_EMAIL
                    send_mail(
                        'Egzamin',
                        'Egzamin o godzine: %s, w sali %s, z kursu: %s, link: https://156.17.42.121%s' % (
                            slug.exam.start_date, exam.class_room, exam.course_group.name, link),
                        settings.EMAIL_HOST_USER,
                        [recip],
                        fail_silently=False,
                    )
                    slug.email_sent = True
                    slug.save()
                    logger.info('email sent to %s' % recip)
            elif key == 'add_student_all':
                students = Student.objects.filter(course_group=exam.course_group, is_blocked=False)
                exam.students.add(*students)
            elif key == 'add_student_re':
                pass
        return JsonResponse({"success": "1", "changed": changed})
    try:
        grades = sorted(exam.grade.items())
    except AttributeError:
        grades = sorted({'3.0': 50, '3.5': 60, '4.0': 70, '4.5': 80, '5.0': 90, '5.5': 95}.items())
    form = ExamEditForm(instance=exam)
    email_button = True if ExamSlug.objects.filter(exam=exam, email_sent=False, student__in=exam.students.all()) else False
    return render(request, 'exams/edit_detail/index.html', {'exam': exam, 'grades': grades, 'form': form, 'email_button': email_button})


def student_panel(request, slug):
    student = get_object_or_404(Student, slug=slug)
    exams = Exam.objects.filter(students=student, slugs__student=student).annotate(
        slug=F('slugs__slug'),
        points=Sum(Case(When(answers__student=student, then=F('answers__result')), output_field=IntegerField())))
    for exam in exams:
        max_points = exam.question_count
        percent_score = (exam.points / max_points) * 100
        try:
            items = sorted({k: v for k, v in exam.grade.items() if v <= percent_score})
        except AttributeError:
            items = None
        exam.student_grade = '2.0' if not items else items[-1]
    return render(request, 'exams/panel/index.html', {'student': student, 'exams': exams})


def exam_panel(request, slug_student, slug_exam):
    exam_slug = get_object_or_404(ExamSlug, slug=slug_exam)
    student = get_object_or_404(Student, slug=slug_student)
    if student != exam_slug.student:
        raise Http404("Strona nie istnieje, sprawdź jeszcze raz link")
    if exam_slug.status == ExamSlug.IN_PROGRESS:
        questions = Answer.objects.filter(student=student, exam=exam_slug.exam).only('answer', 'id')
    else:
        questions = None
    return render(request, 'exams/exam/index.html', {
        'student': student,
        'slug_exam': slug_exam,
        'questions': questions,
        'finished': True if exam_slug.status == ExamSlug.FINISHED else False})


def start_exam_student(request, slug_student, slug_exam):
    exam_slug = get_object_or_404(ExamSlug, slug=slug_exam)
    if exam_slug.status == ExamSlug.IN_PROGRESS and exam_slug.exam.can_start:
        return redirect('exams:exam_question', slug_student, slug_exam, 1)
    return redirect('exams:student_exam_panel', slug_student, slug_exam)


def start_exam_panel(request, exam_id):
    exam = get_object_or_404(Exam, pk=exam_id)
    if request.method == 'POST':
        if 'exam_slug_id' in request.POST:
            exam_slug_id = request.POST['exam_slug_id']
            exam_slug = get_object_or_404(ExamSlug, id=exam_slug_id)
            if exam_slug.status is not ExamSlug.IN_PROGRESS:
                exam_slug.status = ExamSlug.IN_PROGRESS
            else:
                exam_slug.status = ExamSlug.NEW
            exam_slug.save()
            return JsonResponse({"success": "1", })
        if 'add_all' in request.POST:
            for slug in exam.slugs.all():
                slug.status = ExamSlug.IN_PROGRESS
                slug.save()
            return JsonResponse({"success": "1", })
    if exam.can_start is True:
        return redirect('exams:execution', exam.id)
    return render(request, 'exams/start/index.html', {'exam': exam})


def start_exam(request, exam_id):
    exam = get_object_or_404(Exam, pk=exam_id)
    exam.can_start = True
    exam.save()
    return redirect('exams:execution', exam.id)


def execution(request, exam_id):
    import json
    exam = get_object_or_404(Exam, pk=exam_id)
    try:
        grades = json.dumps(exam.grade)
    except AttributeError:
        grades = json.dumps({'3.0': 50, '3.5': 60, '4.0': 70, '4.5': 80, '5.0': 90, '5.5': 95})
    return render(request, 'exams/execution/index.html', {'exam': exam, 'grades': grades})


def end_exam(request, slug_student, slug_exam):
    exam_slug = get_object_or_404(ExamSlug, slug=slug_exam)
    exam_slug.status = ExamSlug.FINISHED
    exam_slug.save()
    return redirect('exams:student_panel', slug_student)


def exam_question(request, slug_student, slug_exam, nr):
    exam_slug = get_object_or_404(ExamSlug, slug=slug_exam)
    student = get_object_or_404(Student, slug=slug_student)
    if student != exam_slug.student:
        raise Http404("Strona nie istnieje, sprawdź jeszcze raz link")
    nr = int(nr)
    q_count = Answer.objects.filter(student=student, exam=exam_slug.exam).count()
    if nr < 1 or nr > q_count:
        raise Http404("Pytanie nie istnieje")
    answer = Answer.objects.filter(student=student, exam=exam_slug.exam)[nr - 1]
    if request.method == 'POST':
        if 'key' in request.POST and 'data' in request.POST:
            choice = answer.question.choices[request.POST['data']]
            if request.POST['key'] == 'check':
                answer.result = int(choice['points'])
                answer.answer = choice['id']
            # else:
            #     answer.result = int(choice['points'])
                answer.save()
        return JsonResponse({"success": "1", })
    selected = '' if answer.answer == '' else int(answer.answer)
    page = {'has_next': 1 if q_count > nr else 0, 'has_prev': 0 if nr <= 1 else 1, 'next': nr + 1, 'prev': nr - 1}
    return render(request, 'exams/question/index.html', {
        'student': student,
        'nr': nr,
        'slug_exam': slug_exam,
        'question': answer.question,
        'page': page,
        'selected': selected,
    })


@login_required
def delete_exam(request, exam_id):
    exam = get_object_or_404(Exam, pk=exam_id)
    exam.delete()
    return redirect('exams:search')


@login_required
def download_exam_csv(request, exam):
    return ExamService.get_exam_csv(exam)
