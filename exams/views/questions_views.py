import hashlib
from datetime import datetime

from django.contrib.auth import get_user
from django.db.models import Max
from django.http import JsonResponse
from django.shortcuts import render, redirect

from exams.forms import UploadFileForm
from exams.models import QuestionSet, Question
from exams.services import ExamService
import json


def edit(request, question_set):
    qs = QuestionSet.objects.get(id=question_set)
    if request.method == 'POST':
        if request.POST.get('question_id'):
            question = Question.objects.get(id=request.POST.get('question_id'))
        else:
            question = Question(question_set=qs)
            question.nr = Question.objects.filter(question_set=qs).aggregate(Max('nr'))['nr__max'] + 1
        question.description = request.POST.get('description')
        question.group = request.POST.get('group')
        question.typ = request.POST.get('typ')
        choices = {
            hashlib.md5(str(datetime.now()).encode('utf-8') + k.encode('utf-8')).hexdigest()[:8]: {
                'answer': v['answer'],
                'id': k,
                'points': v['points']} for k, v in json.loads(request.POST.get('choices')).items()}
        question.choices = choices
        question.save()
        return JsonResponse({"success": "1", })
    return render(request, 'questions/edit/index.html', {'question_set': qs})


def search(request):
    return render(request, 'questions/search/index.html')


def add(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            question_set = ExamService.add_exam_from_dbf(
                get_user(request),
                request.FILES['file'],
                form.cleaned_data['department'],
                form.cleaned_data['course'],
                form.cleaned_data['question_set'])
            return redirect('exams:question_edit', question_set.id)
    return render(request, 'questions/add/index.html')
