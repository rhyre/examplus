from django.contrib.auth import get_user
from django.db.models import F, Q, Sum
from rest_framework import viewsets, filters
from rest_framework.decorators import list_route
from rest_framework.response import Response

from exams.models import Exam, QuestionSet, Question, ExamSlug
from exams.serializers import ExamSerializer, QuestionSetSerializer, \
    QuestionSetAjaxSerializer, QuestionSerializer, ExamSlugSerializer


class ExamFilter(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        name = request.GET.get('name')
        if name:
            queryset = queryset.filter(name__icontains=name)
        return queryset.filter(owner=get_user(request))


class ExamSlugFilter(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if 'in_progress' in request.GET:
            queryset = queryset.filter(status__in=[ExamSlug.IN_PROGRESS, ExamSlug.FINISHED])
        name = request.GET.get('name')
        if name:
            queryset = queryset.filter(
                Q(student__first_name__icontains=name) |
                Q(student__last_name__icontains=name) |
                Q(student__index__icontains=name))
        exam_id = request.GET.get('exam')
        return queryset.filter(exam__id=exam_id)


class QuestionSetFilter(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        name = request.GET.get('name')
        if name:
            queryset = queryset.filter(name__icontains=name)
        return queryset.filter(owner=get_user(request))


class QuestionFilter(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        desctription = request.GET.get('desctription')
        if desctription:
            queryset = queryset.filter(desctription__icontains=desctription)
        question_set = request.GET.get('question_set') or 0
        return queryset.filter(question_set=question_set)


class ExamsViewSet(viewsets.ModelViewSet):
    serializer_class = ExamSerializer
    filter_backends = (filters.OrderingFilter, ExamFilter)

    def get_queryset(self):
        return Exam.objects.filter(owner=get_user(self.request)).annotate(
            course_name=F('course_group__course__name'),
            department=F('course_group__course__department__name'))


class ExamsSlugViewSet(viewsets.ModelViewSet):
    serializer_class = ExamSlugSerializer
    filter_backends = (filters.OrderingFilter, ExamSlugFilter)

    queryset = ExamSlug.objects.annotate(
        student_first_name=F('student__first_name'),
        student_last_name=F('student__last_name'),
        student_index=F('student__index'),
        answers_sum=Sum('answers__result'),)


class QuestionSetViewSet(viewsets.ModelViewSet):
    queryset = QuestionSet.objects.all()
    serializer_class = QuestionSetSerializer
    filter_backends = (filters.OrderingFilter, QuestionSetFilter)

    @list_route()
    def ajax(self, request):
        questionsets = QuestionSet.objects.filter(owner=get_user(request))
        if 'name' in request.GET:
            questionsets = questionsets.filter(name__icontains=request.GET['name'])
        serializer = QuestionSetAjaxSerializer(questionsets, many=True)
        return Response({'results': serializer.data})


class QuestionViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer
    filter_backends = (filters.OrderingFilter, QuestionFilter)
