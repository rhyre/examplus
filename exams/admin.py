from django.contrib import admin

from .models import QuestionSet, Question, Exam, Answer, ExamSlug


admin.site.register(QuestionSet)
admin.site.register(Question)
admin.site.register(Exam)
admin.site.register(Answer)
admin.site.register(ExamSlug)
