#!/usr/bin/env bash
sed -i '' "s#url(https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic&subset=latin)#'/static/libs/fonts/lato.css'#g" /usr/src/app/static/libs/semantic-ui/dist/semantic.min.css
echo "***********************************************"
echo "---django migrate---"
echo "***********************************************"
yes 'yes' | python manage.py migrate
echo "***********************************************"
echo "---django create admin with password pass---"
echo "***********************************************"
echo "from django.contrib.auth.models import User; User.objects.filter(username='admin').exists() or User.objects.create_superuser('admin', 'admin@example.com', 'pass', first_name='Pan', last_name='Admin')" | python manage.py shell
python manage.py runserver 0.0.0.0:8000
