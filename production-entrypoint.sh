#!/usr/bin/env bash
yes "yes" | python3 manage.py collectstatic &
gunicorn examplus.wsgi:application -w 2 -b 0.0.0.0:8000
