from django.forms.widgets import Input, DateTimeInput, Select


class PopupText(Input):
    input_type = 'text'
    template_name = 'django/forms/widgets/popup_text.html'


class PopupDateTime(DateTimeInput):
    input_type = 'text'
    template_name = 'django/forms/widgets/popup_datetime.html'


class PopupSelect(Select):
    input_type = 'text'
    template_name = 'django/forms/widgets/popup_dropdown.html'
