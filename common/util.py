import functools


def rsetattr(obj, attr, val):
    pre, _, post = attr.rpartition('.')
    return setattr(rgetattr(obj, pre) if pre else obj, post, val)


sentinel = object()


def rgetattr(obj, attr, default=sentinel):
    if default is sentinel:
        _getattr = getattr
    else:
        def _getattr(obj, name):
            return getattr(obj, name, default)
    return functools.reduce(_getattr, [obj] + attr.split('.'))

polish_letters = ('ą', 'a'), ('ł', 'l'), ('ś', 's'), ('ć', 'c'), \
                 ('ń', 'n'), ('ź', 'z'), ('ę', 'e'), ('ó', 'o'), ('ż', 'z')


def replace_polish_letters(obj):
    return functools.reduce(lambda a, kv: a.replace(*kv), polish_letters, obj)
