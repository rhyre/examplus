from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from courses.models import Course, CourseGroup, Department, Student
from exams.models import Exam, QuestionSet, ExamSlug


class ExamViewTests(TestCase):

    def setUp(self):
        User.objects.create_user('temporary', 'temporary@gmail.com', 'temporary')
        self.client.login(username='temporary', password='temporary')

    def test_exam_search_view(self):
        response = self.client.get(reverse('exams:search'))
        self.assertEqual(response.status_code, 200)

    def test_exam_add_view(self):
        response = self.client.get(reverse('exams:add'))
        self.assertEqual(response.status_code, 200)

    def test_exam_edit_view(self):
        user = User.objects.last()
        department = Department(name='W4')
        department.save()
        course = Course(name='Matma', department=department)
        course.save()
        course_group = CourseGroup(name='grupa1', course=course, owner=user)
        course_group.save()
        question_set = QuestionSet(name='pytania', owner=user)
        question_set.save()
        exam = Exam(name='frytki 101', course_group=course_group, start_date=timezone.now(), question_set=question_set, owner=user)
        exam.save()
        response = self.client.get(reverse('exams:edit_detail', args=[exam.id]))
        self.assertEqual(response.status_code, 200)


class ExamQuestionViewTests(TestCase):

    def setUp(self):
        User.objects.create_user('temporary', 'temporary@gmail.com', 'temporary')
        self.client.login(username='temporary', password='temporary')

    def test_exam_question_add_view(self):
        response = self.client.get(reverse('exams:question_add'))
        self.assertEqual(response.status_code, 200)

    def test_exam_question_search_view(self):
        response = self.client.get(reverse('exams:question_search'))
        self.assertEqual(response.status_code, 200)

    def test_exam_question_edit_view(self):
        user = User.objects.last()
        department = Department(name='W4')
        department.save()
        course = Course(name='Matma', department=department)
        course.save()
        course_group = CourseGroup(name='grupa1', course=course, owner=user)
        course_group.save()
        question_set = QuestionSet(name='pytania', owner=user)
        question_set.save()
        response = self.client.get(reverse('exams:question_edit', args=[question_set.id]))
        self.assertEqual(response.status_code, 200)

    def test_exam_student_panel_view(self):
        user = User.objects.last()
        department = Department(name='W4')
        department.save()
        course = Course(name='Matma', department=department)
        course.save()
        course_group = CourseGroup(name='grupa1', course=course, owner=user)
        course_group.save()
        student = Student(first_name='Mistrz', last_name='Kodu', slug='slug', index='1222', course_group=course_group)
        student.save()
        response = self.client.get(reverse('exams:student_panel', args=[student.slug]))
        self.assertEqual(response.status_code, 200)

    def test_exam_exam_panel_view(self):
        user = User.objects.last()
        department = Department(name='W4')
        department.save()
        course = Course(name='Matma', department=department)
        course.save()
        course_group = CourseGroup(name='grupa1', course=course, owner=user)
        course_group.save()
        student = Student(first_name='Mistrz', last_name='Kodu', slug='slug', index='1222', course_group=course_group)
        student.save()
        question_set = QuestionSet(name='pytania', owner=user)
        question_set.save()
        exam = Exam(name='frytki 101', course_group=course_group, start_date=timezone.now(), question_set=question_set, owner=user)
        exam.save()
        examslug = ExamSlug(exam=exam, student=student, slug='slug')
        examslug.save()
        response = self.client.get(reverse('exams:student_exam_panel', args=[student.slug, examslug.slug]))
        self.assertEqual(response.status_code, 200)


class ExamApiViewTests(TestCase):

    def setUp(self):
        User.objects.create_user('temporary', 'temporary@gmail.com', 'temporary')
        self.client.login(username='temporary', password='temporary')

    def test_exam_api_view(self):
        response = self.client.get(reverse('api:exams-list'))
        self.assertEqual(response.status_code, 200)

    def test_question_set_api_view(self):
        response = self.client.get(reverse('api:questionset-list'))
        self.assertEqual(response.status_code, 200)

    def test_question_api_view(self):
        response = self.client.get(reverse('api:question-list'))
        self.assertEqual(response.status_code, 200)

    def test_question_set_ajax_api_view(self):
        response = self.client.get(reverse('api:questionset-ajax'))
        self.assertEqual(response.status_code, 200)
