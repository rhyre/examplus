from django.test import TestCase
from django.urls import reverse


class LoginViewTests(TestCase):

    def test_login_view(self):
        response = self.client.get(reverse('login'))
        self.assertEqual(response.status_code, 200)


class HomeViewTests(TestCase):

    def test_home_view(self):
        response = self.client.get(reverse('faq'))
        self.assertEqual(response.status_code, 200)

    def test_api_view(self):
        response = self.client.get(reverse('api:api-root'))
        self.assertEqual(response.status_code, 200)
