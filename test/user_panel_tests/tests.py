from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse


class UserPanelViewTests(TestCase):

    def setUp(self):
        User.objects.create_user('temporary', 'temporary@gmail.com', 'temporary')
        self.client.login(username='temporary', password='temporary')

    def test_panel_view(self):
        response = self.client.get(reverse('user_panel:panel'))
        self.assertEqual(response.status_code, 200)
