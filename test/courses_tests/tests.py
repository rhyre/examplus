from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from courses.models import Course, Department, CourseGroup


class CoursesViewTests(TestCase):

    def setUp(self):
        User.objects.create_user('temporary', 'temporary@gmail.com', 'temporary')
        self.client.login(username='temporary', password='temporary')

    def test_courses_view(self):
        response = self.client.get(reverse('courses:search'))
        self.assertEqual(response.status_code, 200)

    def test_courses_add_view(self):
        response = self.client.get(reverse('courses:add'))
        self.assertEqual(response.status_code, 200)

    def test_course_edit_view(self):
        user = User.objects.last()
        department = Department(name='W4')
        department.save()
        course = Course(name='Matma', department=department)
        course.save()
        course_group = CourseGroup(name='grupa1', course=course, owner=user)
        course_group.save()
        response = self.client.get(reverse('courses:edit_detail', args=[course_group.id]))
        self.assertEqual(response.status_code, 200)


class StudentApiViewTests(TestCase):

    def setUp(self):
        User.objects.create_user('temporary', 'temporary@gmail.com', 'temporary')
        self.client.login(username='temporary', password='temporary')

    def test_students_api_view(self):
        response = self.client.get(reverse('api:student-list'))
        self.assertEqual(response.status_code, 200)
