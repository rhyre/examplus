**install docker**

docker
```
wget -qO- https://get.docker.com/ | sh
```
docker-compose
```
$ curl -L https://github.com/docker/compose/releases/download/1.13.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
$ chmod +x /usr/local/bin/docker-compose
```

---

**development use**:
```
docker-compose build
docker-compose up
```
credentials for dev version
```username - admin```
```password - pass```

---

**production use**:

make sure you are on a swarm manager node

```docker stack deploy --compose-file production.yml exam```

migraion if needed. => ```docker exec -it <container> /bin/sh``` => ```python3 manage.py migrate```

update web => ```docker service update --image rhyre/exam-plus:latest exam_web```