#!/usr/bin/env python
from pip.req import parse_requirements
from setuptools import setup

install_reqs = parse_requirements("requirements.txt", session=False)
required = [str(ir.req) for ir in install_reqs]

setup(install_requires=required,
      name='examplus',
      version='1.3',
      description='Exam +',
      author='Adrian Bykowski',
      author_email='hornetsixone@gmail.com',
      url='localhost:8000',)
