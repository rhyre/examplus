from django import forms
from common.widgets import PopupText
from django.contrib.auth.models import User


class UserEditForm(forms.ModelForm):
    first_name = forms.CharField(widget=PopupText(attrs={'placeholder': 'Imię...'}), max_length=200, label='Imię')
    last_name = forms.CharField(widget=PopupText(attrs={'placeholder': 'Nazwisko...'}), max_length=200, label='Nazwisko')
    email = forms.CharField(widget=PopupText(attrs={'placeholder': 'E-mail...'}), max_length=200, label='Email')

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', )
