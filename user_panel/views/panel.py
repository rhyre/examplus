from django.contrib.auth import get_user
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import render
from user_panel.forms import UserEditForm


def index(request):
    return render(request, 'user_panel/index/index.html')


@login_required
def panel(request):
    if request.method == 'POST':
        user = get_user(request)
        if 'data' in request.POST and 'key' in request.POST:
            setattr(user, request.POST['key'], request.POST['data'])
            user.save()
        return JsonResponse({"success": "1", "changed": request.POST['data']})
    form = UserEditForm(instance=get_user(request))
    return render(request, 'user_panel/panel/index.html', {'form': form})
