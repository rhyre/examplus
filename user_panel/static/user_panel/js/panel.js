$(document).ready(function() {
    $('.popup_btn').popup({
        popup: $(this).find('.popup'),
        on: 'click',
        position: 'left center'
    });
    $('.update_param').click(function() {
        updateParam($(this));
    });
    $('.action > input').on('keyup', function (e) {
        if (e.keyCode == 13) {
            updateParam($(this));
        }
    });
});
