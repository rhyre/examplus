from django.conf.urls import url
from rest_framework import routers

from .views import panel


app_name = 'user_panel'

router = routers.DefaultRouter()


urlpatterns = [
    url(r'^$', panel.panel, name='panel'),
]
