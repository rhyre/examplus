FROM rhyre/python3-alpine-edge-django
COPY requirements-alpine.txt /
RUN pip3 install --no-cache-dir -r requirements-alpine.txt
WORKDIR /usr/src/app
COPY . /usr/src/app
RUN bower install --allow-root
RUN rm -fr static/libs/semantic-ui/examples static/libs/semantic-ui/src static/libs/jquery/src static/libs/semantic-ui-calendar/src
RUN apk del -r python3-dev git build-base nodejs-npm
ENV PYTHONUNBUFFERED 1
